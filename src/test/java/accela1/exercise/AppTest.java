package accela1.exercise;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * @author André
 *
 */

public class AppTest {	

	static WebDriver driver;
	static App app;

	@BeforeClass
	public static void start_test() {

		driver = new FirefoxDriver();			
		app = new App(driver);	

		// Step 1
		driver.get("https://www.buyagift.co.uk/");		
		driver.manage().window().maximize();		
	}

	@Test(priority=1) // means that will be ran firstly.
	public void searchProduct(){			
			
		app.verifyWinBuyagiftVoucher(); // This function verifies if div class is loaded or not.
		
		// Step 2
		app.giftName("Afternoon Tea for Two at Patisserie Valerie with Cake Gift Box");		
	
		app.verifyWinBuyagiftVoucher();		

		app.searchGift();	 
	}

	@Test(priority=2)
	public void verifyCorrectPrice(){	
			
		app.verifyWinBuyagiftVoucher();			

		// Step 3		
		app.clickImage();		
		
		app.verifyWinBuyagiftVoucher();		

		Assert.assertEquals(app.searchPrice(), "£35");		
		
		app.verifyWinBuyagiftVoucher();					

		app.buyNow();	
	}

	@Test(priority=3)
	public void addProductInBasket(){
				
		app.verifyWinBuyagiftVoucher();		

		// Step 4		
		Assert.assertEquals(app.checkBasket(), "You have 1 item(s) in your basket");	
	}

	@Test(priority=4)
	public void addPersonalisedMessage(){
				
		app.verifyWinBuyagiftVoucher();		

		// Step 5		
		app.message();
			
		app.verifyWinBuyagiftVoucher();			      
	}

	@Test(priority=5)
	public void verify_e_voucher_delivery_method(){
			
		app.verifyWinBuyagiftVoucher();		

		// Step 6		
		app.voucher();
			
		app.verifyWinBuyagiftVoucher();					
	}

	@Test(priority=6)
	public void verifyNoAdditionalCharge(){
		
		app.verifyWinBuyagiftVoucher();		

		// Step 7
		Assert.assertEquals(app.deliveryTotal(), "£35.00");
		
		app.verifyWinBuyagiftVoucher();		

		Assert.assertEquals(app.basketSum(), "Afternoon Tea for Two at Patisserie Valerie with Cake Gift Box");
	}			

	@Test(priority=7)
	public void verifyCorrectPriceInBasket(){	
		
		app.verifyWinBuyagiftVoucher();		

		// Step 8
		Assert.assertEquals(app.verifyCorrectPrice(), "£35.00");  
	}

	@Test(priority=8)
	public void paySecurelyNow(){
			
		app.verifyWinBuyagiftVoucher();
		
		// Step 9 - part of operation
		app.paySecurelyNow();		
	}	

	@Test(priority=9)
	public void verifyContinueOperationAsGuest(){
			
		app.verifyWinBuyagiftVoucher();		

		// Step 9 - part of operation			
		app.setEmail();
		
		app.verifyWinBuyagiftVoucher();			

		app.continueAsGuest();
	}	

	@Test(priority=10)
	public void verifyExtraParametersInOrder(){
			
		app.verifyWinBuyagiftVoucher();		

		// Step 10
		app.personalDetails();
	}

	@Test(priority=11)
	public void verifyBillingAddress(){
		
		app.verifyWinBuyagiftVoucher();		

		// Step 11
		app.postCode();
	}

	@Test(priority=12)
	public void verifyErrorMessageWhenFakeCreditCcard(){
			
		app.verifyWinBuyagiftVoucher();
		
		// Step 12
		app.fillPaymentDetails();
			
		app.verifyWinBuyagiftVoucher();			

		app.placeOrder();
			
		app.verifyWinBuyagiftVoucher();			

		Assert.assertEquals(app.verifyErrorMessage(),"The card number is not valid, please check the details and try again.");		
	}

	@AfterClass
	public static void end_test(){

		driver.quit();
	}	
}

