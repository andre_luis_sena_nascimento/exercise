package accela1.exercise;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author André
 *
 */
public class App {	

	WebDriver driver;	

	By var_buygiftvoucher = By.cssSelector("#crmSignUp > div:nth-child(1) > div:nth-child(1) > button:nth-child(1) > i:nth-child(1)");
	By var_gift = By.cssSelector("#search-field");
	By var_searchgift = By.cssSelector("#magnifier-search");
	By var_clickimage = By.cssSelector("img.productlist-productimage:nth-child(2)");	
	By var_price = By.cssSelector("#product-price-current");
	By var_buynow = By.cssSelector("button.btn-transactional:nth-child(1)");
	By var_x = By.id("bag-modal-close");
	By var_checkbasketmessage = By.cssSelector(".basket_no_items");
	By var_persmessage = By.cssSelector("textarea.form-control");
	By var_evoucher = By.cssSelector("#basket_summary > div > div.animate-fadeIn.ng-scope > div > div > div:nth-child(3) > span.cost_price > div.form-group.delivery_options > select");	
	By var_optionfree = By.cssSelector("#basket_summary > div > div.animate-fadeIn.ng-scope > div > div > div:nth-child(3) > span.cost_price > div.form-group.delivery_options > select > option:nth-child(3)");
	By var_deliverytotal = By.cssSelector("div.row:nth-child(8) > span:nth-child(2)");
	By var_basketsummary = By.cssSelector(".summary_prd_ttl");
	By var_correctprice = By.cssSelector(".summary_prd_price");
	By var_pay = By.cssSelector(".pay-secure-now-top");
	By var_email = By.cssSelector("#account_email_field");
	By var_asguest = By.cssSelector("button.login_guest:nth-child(3)");	
	By var_title = By.cssSelector("#titlefield");	
	By var_firstname = By.cssSelector("#firstnamefield");
	By var_lastname = By.cssSelector("#lastnamefield");
	By var_telephonenumber = By.cssSelector("#telephonenumberfield");
	By var_continuedetails = By.cssSelector("button.login_continue:nth-child(2)");
	By var_ukcode = By.cssSelector("#postCodeSearchBilling > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > input:nth-child(1)");
	By var_search = By.cssSelector("#postCodeSearchBilling > div:nth-child(1) > div:nth-child(2) > div:nth-child(3) > button:nth-child(1)");
	By var_billingaddress = By.cssSelector("div.col-xs-12 > select:nth-child(1)");
	By var_selectbillingaddress = By.cssSelector("div.col-xs-12 > select:nth-child(1) > option:nth-child(2)");
	By var_cardholder = By.cssSelector("#cardholdername");	
	By var_cardnumber = By.cssSelector("#cardnumber");
	By var_expiredatemonth = By.cssSelector("#expirymonth");
	By var_expiredateyear = By.cssSelector("#expiryyear");
	By var_cvvnumber = By.cssSelector("#cvvnumber");	
	By var_order = By.cssSelector("#btnPlaceOrderButton");
	By var_invalidcreditcardmessage = By.cssSelector("#chk_step3 > div:nth-child(2) > span:nth-child(1)");

	public App(WebDriver driver){

		this.driver = driver;
	}

	public void verifyWinBuyagiftVoucher(){

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);							

		if (driver.findElements(var_buygiftvoucher).size() > 0){					
			driver.findElement(var_buygiftvoucher).click(); 
			driver.manage().window().maximize(); // Come back to main window to continue test automation.
			System.out.println("Win a Buyagift voucher div class was loaded and removed from test automation.");	
			
		} else{
			System.out.println("Win a Buyagift voucher div class was NOT loaded and NOT removed from test automation.");
		}	
	}		

	public void giftName(String giftname) {

		driver.findElement(var_gift).sendKeys(giftname);
		System.out.println("Method giftName was executed.");
	}

	public void searchGift() {

		// Wait for loading of page web components of interest.
		new WebDriverWait(driver, 5).until(ExpectedConditions.elementToBeClickable(var_searchgift));

		driver.findElement(var_searchgift).click();
		System.out.println("Method searchGift was executed.");
	}

	public void clickImage() {

		// Wait for loading of page web components of interest.
		new WebDriverWait(driver, 5).until(ExpectedConditions.visibilityOfElementLocated(var_clickimage));			

		driver.findElement(var_clickimage).click();	
		System.out.println("Method click_image was executed.");
	}

	public String searchPrice() {		

		// Wait for loading of page web components of interest.
		new WebDriverWait(driver, 5).until(ExpectedConditions.visibilityOfElementLocated(var_price));

		System.out.println("Method searchPrice is being to be executed.");
		return driver.findElement(var_price).getText();
	}

	public void buyNow() {
		
		driver.findElement(var_buynow).submit();
		System.out.println("Method buy_now was executed.");
	}

	public String checkBasket() {

		String check_basket = driver.findElement(var_checkbasketmessage).getText();
		System.out.println("Method check_basket is being to be executed.");
		return check_basket;
	}

	public void message() {

		driver.findElement(var_persmessage).sendKeys("test.");
		System.out.println("Method message was executed.");
	}

	public void voucher() {

		driver.findElement(var_evoucher).click();		
		driver.findElement(var_optionfree).click();	
		System.out.println("Method voucher was executed.");
	}	

	public String deliveryTotal() {

		String total = driver.findElement(var_deliverytotal).getText();
		System.out.println("Method delivery_total is being to be executed.");
		return total;
	}

	public String basketSum() {

		String basket = driver.findElement(var_basketsummary).getText();
		System.out.println("Method basket_sum is being to be executed.");
		
		return basket;
	}

	public String verifyCorrectPrice() {

		String true_price = driver.findElement(var_correctprice).getText();
		System.out.println("Method verify_correct_price is being to be executed.");
		
		return true_price;
	}

	public void paySecurelyNow() {

		driver.findElement(var_pay).click();
		Alert alt = driver.switchTo().alert();
		alt.accept();
		System.out.println("Method pay_securely_now was executed.");
	}

	public void setEmail() {

		// Wait for loading of page web components of interest.
		new WebDriverWait(driver, 5).until(ExpectedConditions.visibilityOfElementLocated(var_email));
		
		driver.findElement(var_email).sendKeys("andre.sena@gmail.com");		
		System.out.println("Method set_email was executed.");
	}

	public void continueAsGuest() {

		driver.findElement(var_asguest).click();
		System.out.println("Method continue_as_guest was executed.");
	}

	public void personalDetails() {

		// Wait for loading of page web components of interest.
		new WebDriverWait(driver, 5).until(ExpectedConditions.visibilityOfElementLocated(var_title));				

		driver.findElement(var_title).sendKeys("Mr.");		
		driver.findElement(var_firstname).sendKeys("Andre");
		driver.findElement(var_lastname).sendKeys("Nascimento");
		driver.findElement(var_telephonenumber).sendKeys("927599974");
		driver.findElement(var_continuedetails).click();	
		System.out.println("Method personal_details was executed.");
	}	

	public void postCode() {

		// Wait for loading of page web components of interest.
		new WebDriverWait(driver, 5).until(ExpectedConditions.visibilityOfElementLocated(var_ukcode));

		driver.findElement(var_ukcode).sendKeys("AB10AA");
		driver.findElement(var_search).click();

		// Wait for loading of page web components of interest.	
		new WebDriverWait(driver, 5).until(ExpectedConditions.visibilityOfElementLocated(var_billingaddress));		

		driver.findElement(var_billingaddress).click();
		driver.findElement(var_selectbillingaddress).click();	
		System.out.println("Method postcode was executed.");
	}		

	public void fillPaymentDetails() {

		driver.findElement(var_cardholder).sendKeys("test");		
		driver.findElement(var_cardnumber).sendKeys("1234123412341234");
		driver.findElement(var_expiredatemonth).sendKeys("3");
		driver.findElement(var_expiredateyear).sendKeys("2018");
		driver.findElement(var_cvvnumber).sendKeys("102");	
		System.out.println("Method fill_payment_details was executed.");
	}

	public void placeOrder() {

		driver.findElement(var_order).click();	
		System.out.println("Method place_order was executed.");
	}

	public String verifyErrorMessage() {

		String message = driver.findElement(var_invalidcreditcardmessage).getText();	
		System.out.println("Method verify_error_message is being to be executed.");
		
		return message;		
	}
}
